package com.sintesistv.sintesistv;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import java.io.File;

/**
 * Created by irnh3 on 23/02/2018.
 */

public abstract class SingleFragmentActivity extends AppCompatActivity {

    protected abstract Fragment createFragment();

    protected  abstract int getFragmentId();

    @LayoutRes
    protected int getLayoutResId(){
        return R.layout.phone;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());

        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(getFragmentId());

        if (fragment == null) {
            fragment = createFragment();
            fm.beginTransaction()
                    .add(getFragmentId(), fragment)
                    .commit();
        }
    }

}
