package com.sintesistv.sintesistv.models;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * Created by irnh3 on 23/02/2018.
 */

public class News {
    private UUID mUUID;
    private String mTitle;
    private String mDescription;
    private Date mDate;
    private String mTemplatePic;

    public News(String title, String description, Date date, String templatePic) {
        mUUID = UUID.randomUUID();
        mTitle = title;
        mDescription = description;
        mDate = date;
        mTemplatePic = templatePic;
    }

    public UUID getUUID() {
        return mUUID;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getDescription() {
        return mDescription;
    }

    public Date getDate() {
        return mDate;
    }

    public String getMainPic() {
        return mTemplatePic;
    }


}
