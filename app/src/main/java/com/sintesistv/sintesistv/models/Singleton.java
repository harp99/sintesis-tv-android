package com.sintesistv.sintesistv.models;

import android.content.Context;
import android.os.AsyncTask;

import com.sintesistv.sintesistv.R;
import com.sintesistv.sintesistv.libs.HttpRequests;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

/**
 * Created by irnh3 on 23/02/2018.
 */

public class Singleton {
    private int mFragment;
    private static Singleton ourInstance;
    private Context mContext;
    private List<News> mNewsList;
    private int page;
    private boolean mEnd;

    private Thread thread;

    public boolean ismAdding() {
        return mAdding;
    }

    public void setmAdding(boolean mAdding) {
        this.mAdding = mAdding;
    }

    private boolean mAdding;
    private String mDate;
    private AsyncTask<String , Void ,String> mHttpRequests;

    private String mCategory;
    private String mSearch;

    public static Singleton get(Context context) {
        if (ourInstance == null){
            ourInstance = new Singleton(context);
        }
        return ourInstance;
    }

    public Thread getThread() {
        return thread;
    }

    public void previousPage() {
        page--;
    }
    public void setThread(Thread thread) {
        this.thread = thread;
    }

    public void reset(){
        ourInstance = null;
    }
    private Singleton(Context context){
        mContext = context.getApplicationContext();
        mCategory = "";
        mSearch = "";
        mDate = "";
        page = 1;
        mEnd = false;
        mAdding = false;
        mFragment = -1;
        mHttpRequests = null;
        thread = new Thread();
        List<News> tempNewsList = GetData("");
        if (tempNewsList != null) {
            mNewsList = tempNewsList;
        }
    }

    public List<News> GetData(final String page) {
        String result = null;
        boolean flag = false;
        String urlString = "https://sintesistv.com.mx/wp-json/wp/v2/posts?" + mDate + page + mCategory + mSearch;
        try {
            try{
                if (mHttpRequests != null){
                    mHttpRequests.cancel(true);
                }
            } catch (Exception e){}

            mHttpRequests = new HttpRequests().execute(urlString);
            result = mHttpRequests.get();
            mHttpRequests = null;
        } catch (InterruptedException e) {
            flag = true;
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        List<News> tempNewsList = null;
        if (!flag) {
            tempNewsList = new ArrayList<>();
            if (result != null) {
                JSONObject jObject = null;
                try {
                    jObject = new JSONObject("{ \"result\" = " + result + "}");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    tempNewsList.addAll(getNewsOfJson(jObject.getJSONArray("result")));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return tempNewsList;
    }

    private Collection<? extends News> getNewsOfJson(JSONArray jArray) {
        mEnd = jArray.length() < 10;

        List<News> newsList = new ArrayList<>();

        if(jArray != null) {
            News news;
            JSONObject item;

            for(int i = 0; i< jArray.length();i++){
                try {
                    item = jArray.getJSONObject(i);

                    news = convertItemToNews(item, i);
                    newsList.add(news);
                    
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
        return newsList;
    }

    private News convertItemToNews(JSONObject item, int n) throws JSONException, ParseException {

        String title = item.getJSONObject("title").getString("rendered");
        title = title.replaceAll("&#\\d*\\;","");


        String content = item.getJSONObject("content").getString("rendered");

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        String dateOfNews = item.getString("date");

        Date date = format.parse(dateOfNews);

        if (this.page == 1 && n == 0){
            String g = Integer.toString(Integer.parseInt(dateOfNews.substring(dateOfNews.length() - 2)) + 1);
            mDate = "before="
                    + dateOfNews.substring(0, dateOfNews.length() - 2)
                    + g;
        }

        String pictureURL = "";
        try{
            JSONObject links = item
                    .getJSONObject("_links");
            JSONArray medias = links
                    .getJSONArray("wp:featuredmedia");
            JSONObject media = medias
                    .getJSONObject(0);
            String href = media
                    .getString("href");

            pictureURL = getPicture(href);

        }catch (Exception e){

        }

        return new News(
                title,
                content,
                date,
                pictureURL
        );
    }

    private String getPicture(String string) throws JSONException, ExecutionException, InterruptedException {
        String result = "";
        result = new HttpRequests().execute(string).get();

        return new JSONObject(result)
                .getJSONObject("guid")
                .getString("rendered");
    }

    public List<News> getNews(){
        return mNewsList;
    }

    public News getNews(UUID uuid) {
        for (News news : mNewsList) {
            if (news.getUUID().equals(uuid)) {
                return news;
            }
        }
        return null;
    }

    public File getMainPic(News news) {
        File filesDir = mContext.getFilesDir();
        return new File(filesDir, String.format("@drawable/%s", news.getMainPic()));
    }

    public void ChangeCategory(String category) {
        mEnd = false;
        page = 1;
        mDate = "";
        mCategory = category;
        List<News> tempNewsList = GetData("");
        if (tempNewsList != null) {
            mNewsList = tempNewsList;
        }
    }
    public void ChangeSearch(String search) {
        mEnd = false;
        page = 1;
        mDate = "";
        mSearch = search;
        List<News> tempNewsList = GetData("");
        if (tempNewsList != null) {
            mNewsList = tempNewsList;
        }
    }

    public void addMore() {
        // add news
        if(!mEnd) {
            page++;
            List<News> tempNewsList = GetData("&page=" + String.valueOf(page));
            if (tempNewsList != null) {
                mNewsList.addAll(tempNewsList);
            }
        }
    }

    public int getFragment() {
        return mFragment;
    }
    public void setFragment(int fragmentId) {
        mFragment = fragmentId;
    }
}
