package com.sintesistv.sintesistv;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.view.MenuItem;

import com.sintesistv.sintesistv.Loading.LoadingFragment;
import com.sintesistv.sintesistv.Menu.MenuActivity;
import com.sintesistv.sintesistv.Menu.MenuFragment;
import com.sintesistv.sintesistv.New.NewFragment;
import com.sintesistv.sintesistv.New.NewPagerActivity;
import com.sintesistv.sintesistv.News.NewsActivity;
import com.sintesistv.sintesistv.News.NewsFragment;
import com.sintesistv.sintesistv.models.News;

public class MainActivity extends SingleFragmentActivity implements  NewsFragment.Callbacks{

    @Override
    protected Fragment createFragment() {
        return new LoadingFragment();
    }

    @Override
    protected int getFragmentId() {
        return R.id.main_fragment_container;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }
    @Override
    public void onNewsSelected(News news) {
        Intent intent = NewPagerActivity.newIntent(this, news.getUUID());
        startActivity(intent);
    }
}
