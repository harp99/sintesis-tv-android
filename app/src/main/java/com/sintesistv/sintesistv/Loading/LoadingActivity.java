package com.sintesistv.sintesistv.Loading;

import android.support.v4.app.Fragment;

import com.sintesistv.sintesistv.R;
import com.sintesistv.sintesistv.SingleFragmentActivity;

public class LoadingActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment() {
        return new LoadingFragment();
    }

    @Override
    protected int getFragmentId() {
        return R.id.main_fragment_container;
    }
}
