package com.sintesistv.sintesistv.Loading;

import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sintesistv.sintesistv.Menu.MenuFragment;
import com.sintesistv.sintesistv.R;
import com.sintesistv.sintesistv.models.Singleton;

public class LoadingFragment extends Fragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new Thread(new Runnable() {
            public void run() {

                Singleton.get(getContext()).getNews();

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.main_fragment_container, new MenuFragment());
                        //ft.addToBackStack(null);
                        ft.commit();
                    }
                });
            }
        }).start();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.loading, container, false);

        return view;
    }
}
