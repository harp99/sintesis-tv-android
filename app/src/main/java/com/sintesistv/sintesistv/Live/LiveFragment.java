package com.sintesistv.sintesistv.Live;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.sintesistv.sintesistv.R;
import com.sintesistv.sintesistv.models.Singleton;

/**
 * Created by irnh3 on 25/02/2018.
 */

public class LiveFragment extends Fragment {
    public static final String mVideoUrl = "https://iframe.dacast.com/b/63852/c/88933";
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.live, container, false);


        WebView webView = view.findViewById(R.id.web_view_live);

        webView.getSettings().setAppCachePath(getActivity().getCacheDir().getAbsolutePath() );
        webView.getSettings().setAllowFileAccess( true );
        webView.getSettings().setAppCacheEnabled( true );
        webView.getSettings().setJavaScriptEnabled( true );

        webView.getSettings().setCacheMode( WebSettings.LOAD_DEFAULT );

        webView.loadUrl(mVideoUrl);
        webView.setWebViewClient(new WebViewClient());
        return view;
    }
}
