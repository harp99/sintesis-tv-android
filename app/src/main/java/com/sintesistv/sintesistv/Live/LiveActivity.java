package com.sintesistv.sintesistv.Live;

import android.support.v4.app.Fragment;

import com.sintesistv.sintesistv.R;
import com.sintesistv.sintesistv.SingleFragmentActivity;

/**
 * Created by irnh3 on 25/02/2018.
 */

public class LiveActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment() {
        return new LiveFragment();
    }

    @Override
    protected int getFragmentId() {
        return R.id.menu_fragment_container;
    }
}
