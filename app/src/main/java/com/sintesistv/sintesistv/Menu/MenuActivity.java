package com.sintesistv.sintesistv.Menu;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.sintesistv.sintesistv.New.NewPagerActivity;
import com.sintesistv.sintesistv.News.NewsActivity;
import com.sintesistv.sintesistv.News.NewsFragment;
import com.sintesistv.sintesistv.R;
import com.sintesistv.sintesistv.SingleFragmentActivity;
import com.sintesistv.sintesistv.models.News;

import java.util.UUID;

/**
 * Created by irnh3 on 24/02/2018.
 */

public class MenuActivity extends SingleFragmentActivity implements  NewsFragment.Callbacks {

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, MenuActivity.class);
        return intent;
    }

    @Override
    protected Fragment createFragment() {
        return new MenuFragment();
    }

    @Override
    protected int getFragmentId() {
        return R.id.main_fragment_container;
    }

    @Override
    public void onNewsSelected(News news) {
        new NewsActivity().onNewsSelected(news);
    }
}
