package com.sintesistv.sintesistv.Menu;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.sintesistv.sintesistv.Contact.ContactFragment;
import com.sintesistv.sintesistv.Live.LiveFragment;
import com.sintesistv.sintesistv.New.NewPagerActivity;
import com.sintesistv.sintesistv.News.NewsActivity;
import com.sintesistv.sintesistv.News.NewsFragment;
import com.sintesistv.sintesistv.R;
import com.sintesistv.sintesistv.models.News;
import com.sintesistv.sintesistv.models.Singleton;

import java.io.File;
import java.util.Objects;
import java.util.UUID;

/**
 * Created by irnh3 on 24/02/2018.
 */

public class MenuFragment extends Fragment  {

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            return selectedFragment(getActivity().getSupportFragmentManager(), item.getItemId());
        }
    };

    private boolean selectedFragment(FragmentManager fm, int id) {
        Singleton singleton = Singleton.get(getContext());
        int i = singleton.getFragment();

        if(id != i) {
            Fragment fragment = null;
            switch (id) {
                case R.id.navigation_home:
                    fragment = new NewsFragment();
                    break;
                case R.id.navigation_live:
                    fragment = new LiveFragment();
                    break;
                case R.id.navigation_contact:
                    fragment = new ContactFragment();
                    break;
                default:
                    return false;
            }

            if(i == R.id.navigation_home) {

                try {
                    getView().findViewById(R.id.loading_news).setVisibility(View.GONE);
                }
                catch (Exception ex){
                }

                singleton.setmAdding(false);
            }

            singleton.setFragment(id);

            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.menu_fragment_container, fragment);

            try {
                ft.detach(this).attach(this).commit();
            }catch (Exception e){}
        }
        return  true;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_main, container, false);

        BottomNavigationView navigation = view.findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        // create inner fragment
        selectedFragment(getActivity().getSupportFragmentManager(), Singleton.get(getActivity()).getFragment() != -1? Singleton.get(getActivity()).getFragment() : R.id.navigation_home);

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Singleton.get(getActivity()).reset();
        getActivity().finish();
    }
}
