package com.sintesistv.sintesistv.New;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;

import com.sintesistv.sintesistv.R;
import com.sintesistv.sintesistv.models.News;
import com.sintesistv.sintesistv.models.Singleton;

import java.util.List;
import java.util.UUID;

/**
 * Created by irnh3 on 23/02/2018.
 */

public class NewPagerActivity extends AppCompatActivity {
    private static final String EXTRA_NEWS_ID = "newsId";

    private ViewPager mViewPager;
    private List<News> mNewsList;

    public static Intent newIntent(Context context, UUID newsId) {
        Intent intent = new Intent(context, NewPagerActivity.class);
        intent.putExtra(EXTRA_NEWS_ID, newsId);
        return intent;
    }

    private void AddMoreAsync(Singleton singleton){
        final Singleton singleton1 = singleton;

        final ProgressBar pb = this
                .findViewById(R.id.loading_news);

        try {
            pb.setVisibility(View.VISIBLE);
            pb.bringToFront();
        } catch (Exception e){}

        new Thread(new Runnable() {
            @Override
            public void run() {
                singleton1.addMore();

                try {
                    singleton1.setmAdding(true);
                    pb.setVisibility(View.GONE);
                } catch (Exception e){}
            }
        }).run();
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.pager);

        UUID crimeId = (UUID) getIntent().getSerializableExtra(EXTRA_NEWS_ID);

        mViewPager = findViewById(R.id.news_view_pager);


        final Singleton singleton = Singleton.get(this);
        mNewsList = singleton.getNews();

        FragmentManager fragmentManager = getSupportFragmentManager();

        InfiniteAdapter infiniteAdapter = new InfiniteAdapter(fragmentManager, singleton);
        mViewPager.setAdapter(infiniteAdapter);
        for (int i = 0; i < mNewsList.size(); i++) {
            if (mNewsList.get(i).getUUID().equals(crimeId)) {

                // si es el ultimo elemento de la lista
                if (i  == mNewsList.size() - 1){

                    // esperar a un thread si es que hay
                    try {
                        singleton.getThread().join();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                mViewPager.setCurrentItem(i);
                break;
            }
        }
    }

    class InfiniteAdapter extends FragmentStatePagerAdapter {
        Singleton mSingleton;
        public InfiniteAdapter(FragmentManager fragmentManager, Singleton singleton){
            super(fragmentManager);
            mSingleton = singleton;
        }

        @Override
        public Fragment getItem(int position) {

            // si es el ultimo elemento de la lista
            if (position  == mNewsList.size() - 1){

                if (mSingleton.getThread().isAlive()) {
                    // esperar a un thread si es que hay
                    try {
                        mSingleton.getThread().join();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    mSingleton.setmAdding(true);
                    AddMoreAsync(mSingleton);
                }
            }
            News news = mSingleton.getNews().get(position);
            return NewFragment.newInstance(news.getUUID());
        }

        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }


        @Override
        public int getCount() {
            return Integer.MAX_VALUE;
        }
    }
}
