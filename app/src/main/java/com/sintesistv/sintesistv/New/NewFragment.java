package com.sintesistv.sintesistv.New;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.sintesistv.sintesistv.R;
import com.sintesistv.sintesistv.models.News;
import com.sintesistv.sintesistv.models.Singleton;
import com.squareup.picasso.Picasso;

import java.util.UUID;

/**
 * Created by irnh3 on 23/02/2018.
 */

public class NewFragment extends Fragment {

    private static String ARG_NEWS_ID = "newsId";
    private News mNews;
    private TextView mTitle;
    private ImageView mMainPic;
    private TextView mDate;
    private WebView mDetails;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UUID newsId = (UUID) getArguments().getSerializable(ARG_NEWS_ID);
        mNews = Singleton.get(getActivity()).getNews(newsId);
    }


    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View itemView = inflater.inflate(R.layout.news_layout, container, false);

        mTitle = itemView.findViewById(R.id.text_Title);
        mDetails = itemView.findViewById(R.id.text_description);
        mDate = itemView.findViewById(R.id.text_date);
        mMainPic = itemView.findViewById(R.id.image_view_main_pic);

        mTitle.setText(mNews.getTitle());
        String dateFormat = "dd/LL/yyyy";
        String dateString = DateFormat.format(dateFormat, mNews.getDate()).toString();

        mDate.setText(dateString);


        String content = mNews.getDescription();
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();

        content = content.replaceAll("width=\"\\d*\"","width=\""
                + ((int)(metrics.widthPixels / metrics.scaledDensity) - 12)
                +"\"");
        content = content.replaceAll("height=\"\\d*\"","width=\""
                + ((int)(metrics.widthPixels / metrics.scaledDensity / 4 * 3) - 9)
                +"\"");
        content += "<br />";
        content = "<p>" + content + "</p>";

        mDetails.getSettings().setAppCachePath(getActivity().getCacheDir().getAbsolutePath() );
        mDetails.getSettings().setAllowFileAccess( true );
        mDetails.getSettings().setAppCacheEnabled( true );
        mDetails.getSettings().setJavaScriptEnabled( true );

        mDetails.getSettings().setCacheMode( WebSettings.LOAD_DEFAULT );

        mDetails.loadData(content, "text/html; charset=utf-8", "utf-8");

        // set image

        if(!mNews.getMainPic().isEmpty()) {
            Picasso.get().load(mNews.getMainPic()).into(mMainPic);
        }

        return itemView;
    }

    public static NewFragment newInstance(UUID crimeId){
        Bundle args = new Bundle();

        args.putSerializable(ARG_NEWS_ID, crimeId);

        NewFragment newFragment = new NewFragment();
        newFragment.setArguments(args);
        return newFragment;
    }
}
