package com.sintesistv.sintesistv.News;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Layout;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.io.File;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.sintesistv.sintesistv.MainActivity;
import com.sintesistv.sintesistv.New.NewFragment;
import com.sintesistv.sintesistv.R;
import com.sintesistv.sintesistv.SingleFragmentActivity;
import com.sintesistv.sintesistv.libs.PictureUtils;
import com.sintesistv.sintesistv.models.News;
import com.sintesistv.sintesistv.models.Singleton;
import com.squareup.picasso.Picasso;

/**
 * Created by irnh3 on 23/02/2018.
 */

public class NewsFragment extends Fragment {
    private RecyclerView mNewsRecyclerView;
    private NewsAdapter mNewsAdapter;
    private Callbacks mCallbacks;
    private int mItemsAdded = 0;
    private boolean mLoading;
    private boolean mSearchSelected;
    private Thread mScrolling;
    private Thread mSwitching;
    private View mView;
    private String mCategory;
    private String mSearch;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallbacks = (Callbacks) context;
    }
    @Override
    public void  onCreate(Bundle bundle){
        super.onCreate(bundle);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.layout_news_list, container, false);

        mLoading = false;
        mSearchSelected = false;

        mScrolling = null;
        mSwitching = null;

        loadDisplay();

        return mView;
    }

    private class updateTimer extends TimerTask{

        @Override
        public void run() {
            UpdateData(null);
        }
    }

    public void UpdateData(Singleton singleton) {
        if(singleton == null){
          singleton = Singleton.get(getActivity());
        }

        List<News> news = singleton.getNews();
        if(mNewsAdapter == null) {
            mNewsAdapter = new NewsAdapter(news);
            mNewsRecyclerView.setAdapter(mNewsAdapter);

        }
        else{
            mNewsAdapter.setNewsList(news);
            mNewsAdapter.notifyItemInserted(mItemsAdded);
            mItemsAdded -= mItemsAdded;
        }
    }

    private class NewsHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private News mNews;

        // 2 is for the first page
        private TextView mTitle, mTitle2;
        private TextView mDate, mDate2;
        private ImageView mMainPic, mMainPic2;
        private RelativeLayout mRelativeLayout, mRelativeLayout2;

        private NewsHolder(LayoutInflater layoutInflater, ViewGroup parent) {
            super(layoutInflater.inflate(R.layout.layout_news_element, parent, false));

            mTitle = itemView.findViewById(R.id.text_Title);
            mDate = itemView.findViewById(R.id.text_date);
            mMainPic = itemView.findViewById(R.id.image_view_main_pic);
            mRelativeLayout = itemView.findViewById(R.id.next_layout);

            mTitle2 = itemView.findViewById(R.id.text_Title2);
            mDate2 = itemView.findViewById(R.id.text_date2);
            mMainPic2 = itemView.findViewById(R.id.image_view_main_pic2);
            mRelativeLayout2 = itemView.findViewById(R.id.first_layout);

            itemView.setOnClickListener(this);
        }

        private void bind(News news, Boolean first){
            mNews = news;
            String dateFormat = "dd/LL/yyyy";
            String dateString = DateFormat.format(dateFormat, mNews.getDate()).toString();
            if(first) {
                mRelativeLayout.setVisibility(View.GONE);
                mRelativeLayout2.setVisibility(View.VISIBLE);

                mTitle2.setText(mNews.getTitle());
                mDate2.setText(dateString);
                // set image
                if (!mNews.getMainPic().isEmpty()) {
                    Picasso.get().load(mNews.getMainPic()).into(mMainPic2);
                }
            }
            else{
                mRelativeLayout2.setVisibility(View.GONE);
                mRelativeLayout.setVisibility(View.VISIBLE);

                mTitle.setText(mNews.getTitle());
                mDate.setText(dateString);
                // set image
                if (!mNews.getMainPic().isEmpty()) {
                    Picasso.get().load(mNews.getMainPic()).into(mMainPic);
                }
            }

        }

        @Override
        public void onClick(View v) {
            if(!mLoading)
            {
                mCallbacks.onNewsSelected(mNews);
            }
        }
    }

    private class NewsAdapter extends RecyclerView.Adapter<NewsHolder>{
        private List<News> mNewsList;

        private NewsAdapter(List<News> newsList){
            mNewsList = newsList;
        }

        @Override
        public NewsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            return new NewsHolder(layoutInflater, parent);
        }

        @Override
        public void onBindViewHolder(NewsHolder holder, int position) {
            // check if its the last item;
            final ProgressBar pb = getActivity()
                    .findViewById(R.id.loading_news);
            final Singleton singleton = Singleton.get(getActivity());

            if(position == mNewsList.size() - 1 && !mLoading && !singleton.ismAdding() && !singleton.getThread().isAlive()) {
                // loading
                singleton.setmAdding(true);
                try {
                    pb.setVisibility(View.VISIBLE);
                    pb.bringToFront();
                } catch (Exception e){}
                mScrolling = new Thread(new Runnable() {
                    public void run() {

                        singleton.addMore();
                        mItemsAdded += 10;

                        try {
                            singleton.setmAdding(false);
                        } catch (Exception e){}

                        // not loading
                        try {
                            pb.setVisibility(View.GONE);
                        } catch (Exception e){}
                    }
                });
                singleton.setThread(mScrolling);
                mScrolling.start();
            }
            else{
                if(!mLoading && !singleton.ismAdding()) {
                    // not loading
                    try {

                        pb.setVisibility(View.GONE);
                    } catch (Exception e) {
                    }
                }
            }

            News news = mNewsList.get(position);
            holder.bind(news, position == 0);
        }

        @Override
        public int getItemCount() {
            return mNewsList.size();
        }

        private void setNewsList(List<News> newsList){
            mNewsList = newsList;
        }
        public void addNews(News news){
            mNewsList.add(news);
        }

    }

    public interface Callbacks{
        void onNewsSelected(News news);
    }

    public static NewFragment newInstance(){
        Bundle args = new Bundle();
        NewFragment newFragment = new NewFragment();
        newFragment.setArguments(args);
        return newFragment;
    }

    // options menu

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        final Singleton singleton = Singleton.get(getActivity());
        inflater.inflate(R.menu.categories_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setOnQueryTextListener(
                new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        if (!query.isEmpty()) {
                            mSearch = "&search=" + query;
                        }
                        else{
                            mSearch = "";
                        }
                        RefreshThreadSearch();
                        return false;
                    }


                    @Override
                    public boolean onQueryTextChange(String newText) {
                        if (mSearchSelected){
                            mSearchSelected = false;
                        }
                        else if (newText.isEmpty()) {
                            mSearch = "";
                            RefreshThreadSearch();
                        }
                        return false;
                    }
                }
        );
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() != R.id.search) {
            mCategory = "";
            switch (item.getItemId()) {
                case R.id.category_sports:
                    mCategory = "&categories=48";
                    break;
                case R.id.category_entertainment:
                    mCategory = "&categories=44";
                    break;
                case R.id.category_life_style:
                    mCategory = "&categories=3396";
                    break;
                case R.id.category_news:
                    mCategory = "&categories=45";
                    break;
                case R.id.category_programs:
                    mCategory = "&categories=32";
                    break;
                case R.id.category_technology:
                    mCategory = "&categories=348";
                    break;
            }
            RefreshThreadCategory();
        }
        else{
            mSearchSelected = true;
        }
        return  super.onOptionsItemSelected(item);
    }

    private void RefreshThreadSearch(){
        mLoading = true;
        final Singleton singleton = Singleton.get(getActivity());
        final ProgressBar pb = getActivity()
                .findViewById(R.id.loading_news);

        try {
            if (mScrolling != null
                    && mScrolling.isAlive()){
                mScrolling.interrupt();
            }
        } catch (Exception e){}
        try {
            if (mSwitching != null){
                mSwitching.interrupt();
            }
        } catch (Exception e){}

        try {
            pb.setVisibility(View.VISIBLE);
            pb.bringToFront();
        } catch (Exception e){}
        mSwitching = new Thread(new Runnable() {
            @Override
            public void run() {

                singleton.ChangeSearch(mSearch);
                mItemsAdded = 0;

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(Singleton.get(getActivity()).getFragment() == R.id.main_fragment_container) {
                            mNewsAdapter = null;
                            UpdateData(singleton);
                            mNewsAdapter.notifyDataSetChanged();
                        }
                        pb.setVisibility(View.GONE);
                    }
                });

                mLoading = false;
                mSwitching = null;
            }
        });
        mSwitching.start();
    }

    private void RefreshThreadCategory(){
        mLoading = true;
        final Singleton singleton = Singleton.get(getActivity());
        final ProgressBar pb = getActivity()
                .findViewById(R.id.loading_news);
        try {
            if (mScrolling != null
                    && mScrolling.isAlive()){
                mScrolling.interrupt();
            }
        } catch (Exception e){}
        try {
            if (mSwitching != null){
                mSwitching.interrupt();
            }
        } catch (Exception e){}


        try {
            pb.setVisibility(View.VISIBLE);
            pb.bringToFront();
        } catch (Exception e){}

        mSwitching = new Thread(new Runnable() {
            @Override
            public void run() {

                singleton.ChangeCategory(mCategory);
                mItemsAdded = 0;

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pb.setVisibility(View.GONE);
                        if(Singleton.get(getActivity()).getFragment() == R.id.main_fragment_container) {
                            mNewsAdapter = null;
                            UpdateData(singleton);
                            mNewsAdapter.notifyDataSetChanged();
                        }
                    }
                });


                mLoading = false;
                mSwitching = null;
            }
        });
        mSwitching.start();
    }

    private void loadDisplay() {
        mNewsAdapter = null;
        mNewsRecyclerView = mView.findViewById(R.id.recycle_news_list);
        mNewsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mNewsRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(mItemsAdded > 0){
                    UpdateData(null);
                }
            }
        });
        mNewsRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                if(mItemsAdded > 0){
                    UpdateData(null);
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

        UpdateData(null);
    }
}
