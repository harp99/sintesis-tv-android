package com.sintesistv.sintesistv.News;

import android.app.Activity;
import android.support.v4.app.Fragment;

import com.sintesistv.sintesistv.New.NewFragment;
import com.sintesistv.sintesistv.R;
import com.sintesistv.sintesistv.SingleFragmentActivity;
import com.sintesistv.sintesistv.models.News;

/**
 * Created by irnh3 on 23/02/2018.
 */

public class NewsActivity extends SingleFragmentActivity implements NewsFragment.Callbacks {

    @Override
    public void onNewsSelected(News news) {
            Fragment fragment = NewFragment.newInstance(news.getUUID());
            getSupportFragmentManager().beginTransaction().replace(R.id.main_fragment_container, fragment).commit();
    }

    @Override
    protected Fragment createFragment() {
        return new NewsFragment();
    }

    @Override
    protected int getFragmentId() {
        return R.id.menu_fragment_container;
    }

}
